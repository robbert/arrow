#[macro_use]
extern crate criterion;

extern crate arrow;
extern crate datafusion;

use criterion::Criterion;
use std::cell::RefCell;
use std::env;
use std::rc::Rc;

use arrow::datatypes::{DataType, Field, Schema};
use arrow::record_batch::RecordBatch;
use datafusion::datasource::{CsvFile, MemTable};
use datafusion::execution::context::ExecutionContext;

pub fn run_query(ctx: &mut ExecutionContext, sql: &str) -> Vec<RecordBatch> {
    ctx.sql(&sql, 1024 * 1024).unwrap()
}

fn create_context() -> ExecutionContext {
    let schema = Schema::new(vec![
        Field::new("vendor_id", DataType::Utf8, false),
        Field::new(
            "pickup_datetime",
            DataType::Utf8,
            // DataType::Timestamp(TimeUnit::Second),
            false,
        ),
        Field::new(
            "dropoff_datetime",
            DataType::Utf8,
            // DataType::Timestamp(TimeUnit::Second),
            false,
        ),
        Field::new("passenger_count", DataType::Int32, false),
        Field::new("trip_distance", DataType::Float32, false),
        Field::new("pickup_longitude", DataType::Float32, false),
        Field::new("pickup_latitude", DataType::Float32, false),
        Field::new("rate_code", DataType::Int32, false),
        Field::new("store_and_fwd_flag", DataType::Int32, true),
        Field::new("dropoff_longitude", DataType::Float32, false),
        Field::new("dropoff_latitude", DataType::Float32, false),
        Field::new("payment_type", DataType::Utf8, false),
        Field::new("fare_amount", DataType::Float64, false),
        Field::new("surcharge", DataType::Float64, false),
        Field::new("mta_tax", DataType::Float64, false),
        Field::new("tip_amount", DataType::Float64, false),
        Field::new("tolls_amount", DataType::Float64, false),
        Field::new("total_amount", DataType::Float64, false),
    ]);

    // Loading the entire CSV file in memory allows us to test the throughput
    // without benchmarking disk IO
    let data_dir = env::var("ARROW_TEST_DATA").expect("ARROW_TEST_DATA not defined");

    let csv = CsvFile::new(
        &format!("{}/yellow_tripdata_2010-01.csv", data_dir),
        &schema,
        true,
    );

    let mut ctx = ExecutionContext::new();

    let mem_table = MemTable::load(&csv).unwrap();
    ctx.register_table("trip_2010_01", Rc::new(mem_table));

    ctx
}

fn nyc_benchmark(c: &mut Criterion) {
    let ctx = Rc::new(RefCell::new(create_context()));

    {
        let ctx = ctx.clone();

        c.bench_function("complex_aggregate_query", move |b| {
            b.iter(|| {
                run_query(
                    &mut ctx.borrow_mut(),
                    "SELECT passenger_count, MIN(fare_amount), MAX(fare_amount) \
                     FROM trip_2010_01 GROUP BY passenger_count \
                     HAVING AVG(total_amount) > 1.69",
                )
            })
        });
    }

    // The query execution engine sadly doesn't support filtring on strings
    c.bench_function("simple_filtering_query", move |b| {
        b.iter(|| {
            run_query(
                &mut ctx.borrow_mut(),
                "SELECT vendor_id, fare_amount, total_amount \
                 FROM trip_2010_01 \
                 WHERE tip_amount > 0 OR tolls_amount <> 0",
            )
        })
    });
}

// The data set is huge, so we have to limit the number of samples
criterion_group!(
    name = benches;
    config = Criterion::default().sample_size(20);
    targets = nyc_benchmark
);
criterion_main!(benches);
