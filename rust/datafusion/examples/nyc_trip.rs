use std::env;
use std::cell::RefCell;
use std::rc::Rc;
use std::time::Instant;

extern crate arrow;
extern crate datafusion;

use arrow::array::{Float64Array, Int32Array, UInt32Array};
use arrow::record_batch::RecordBatch;
use arrow::datatypes::{DataType, Field, Schema};

use datafusion::error::Result;
use datafusion::datasource::{CsvFile, MemTable};
use datafusion::execution::context::ExecutionContext;

fn main() -> Result<()> {
    let now = Instant::now();
    println!("Loading the data into an execution context");
    let ctx = Rc::new(RefCell::new(create_context()));
    show_elapsed_time(now, "Creating the context");

    for i in 0..100 {
        println!("Running query number {}", i);
        run_aggregate_query(&ctx);
    }
    Ok(())
}

fn run_aggregate_query(ctx: &Rc<RefCell<ExecutionContext>>){
    let now = Instant::now();
    let mut mut_ctx = ctx.borrow_mut();

    // Create a query plan and execute it
    let sql = "SELECT passenger_count, MIN(fare_amount), MAX(fare_amount) \
        FROM trip_2010_01 GROUP BY passenger_count \
        HAVING AVG(total_amount) > 1.69";

    let results = mut_ctx.sql(&sql, 1024 * 1024).unwrap();
    results.iter().for_each(show_batch);
    show_elapsed_time(now, "Executing the query");
}

fn create_context() -> ExecutionContext {
    let schema = Schema::new(vec![
        Field::new("vendor_id", DataType::Utf8, false),
        Field::new(
            "pickup_datetime",
            DataType::Utf8,
            // DataType::Timestamp(TimeUnit::Second),
            false,
        ),
        Field::new(
            "dropoff_datetime",
            DataType::Utf8,
            // DataType::Timestamp(TimeUnit::Second),
            false,
        ),
        Field::new("passenger_count", DataType::Int32, false),
        Field::new("trip_distance", DataType::Float32, false),
        Field::new("pickup_longitude", DataType::Float32, false),
        Field::new("pickup_latitude", DataType::Float32, false),
        Field::new("rate_code", DataType::Int32, false),
        Field::new("store_and_fwd_flag", DataType::Int32, true),
        Field::new("dropoff_longitude", DataType::Float32, false),
        Field::new("dropoff_latitude", DataType::Float32, false),
        Field::new("payment_type", DataType::Utf8, false),
        Field::new("fare_amount", DataType::Float64, false),
        Field::new("surcharge", DataType::Float64, false),
        Field::new("mta_tax", DataType::Float64, false),
        Field::new("tip_amount", DataType::Float64, false),
        Field::new("tolls_amount", DataType::Float64, false),
        Field::new("total_amount", DataType::Float64, false),
    ]);

    // Loading the entire CSV file in memory allows us to test the throughput/home/gijs/uni/mov
    // without benchmarking disk IO
    let data_dir = env::var("ARROW_TEST_DATA").expect("ARROW_TEST_DATA not defined");

    let csv = CsvFile::new(
        &format!("{}/yellow_tripdata_2010-01.csv", data_dir),
        &schema,
        true,
    );

    let mut ctx = ExecutionContext::new();

    let mem_table = MemTable::load(&csv).unwrap();
    ctx.register_table("trip_2010_01", Rc::new(mem_table));

    ctx
}

fn show_batch(batch: &RecordBatch) {
    for row in 0..batch.num_rows() {
        let mut line = Vec::with_capacity(batch.num_columns());
        for col in 0..batch.num_columns() {
            let array = batch.column(col);
            match array.data_type() {
                DataType::Int32 => {
                    let array = array.as_any().downcast_ref::<Int32Array>().unwrap();
                    line.push(format!("{}", array.value(row)));
                }
                DataType::UInt32 => {
                    let array = array.as_any().downcast_ref::<UInt32Array>().unwrap();
                    line.push(format!("{}", array.value(row)));
                }
                DataType::Float64 => {
                    let array = array.as_any().downcast_ref::<Float64Array>().unwrap();
                    line.push(format!("{}", array.value(row)));
                }
                other => {
                    line.push(format!("unsupported type {:?}", other));
                }
            }
        }
        println!("{:?}", line);
    }
}

fn show_elapsed_time(now: Instant, what: &str){
    let duration = now.elapsed();
    let seconds = duration.as_secs() as f64 + (duration.subsec_nanos() as f64 / 1000000000.0);
    println!("{} took {:.4} seconds", what, seconds);

}