#!/usr/bin/env bash
#
# A quick wrapper to set up benchmark data. All of this is already made clear in
# the readme, this just automates thep rocess.

cd "$(dirname "$0")"

git submodule update --init

export ARROW_TEST_DATA=$(realpath ../testing/data/)
export PARQUET_TEST_DATA=$(realpath ../cpp/submodules/parquet-testing/data)

nyc_data_path=$ARROW_TEST_DATA/yellow_tripdata_2010-01.csv
if [[ ! -e $nyc_data_path ]]; then
  echo "Missing NYC testing data, downloading after 5 seconds..."
  echo "WARNING: The file is 2.6 GB in size"
  sleep 5

  # Ignoring the testing data
  echo "yellow_tripdata_2010-01.csv" >>../.git/modules/testing/info/exclude

  wget -O "$nyc_data_path" "https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_2010-01.csv"
  # Might be nice to run dos2unix on this file to get clean line
  # endings. Second line is also emtpy for no reason, use
  # `sed -i '2d' yellow_tripdata_2010-01.csv` to remove it.
fi

# Compile with debug symbols. This might prevent some optimizations but it's
# really useful for profiling.
export RUSTFLAGS=-g

exec cargo "$@"
