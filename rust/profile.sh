#!/usr/bin/env bash
#
# Profiling through Intel VTune can be done by first compiling the benchmarks
# with release symbols like so:
#     env RUSTFLAGS=-g cargo build --release --bench nyc_trip_csv
#
# And then configuring the application in VTune with the following parameters:
#   - Application: ./cargo.sh
#   - Application parameters: bench --bench nyc_trip_csv -- --profile-time 20
#
# Make sure to manually recompile the application before profiling to prevent
# the compilation process from cluttering the results.

# Build in advance to prevent the build from interfering with the profiling
# results
./cargo.sh build --release --bench nyc_trip_csv

# LBR is also a good alternative to DWARF. It doesn't capture the entire call
# stack but that might be useful at times.
exec perf record --call-graph=dwarf --aio "$@" ./cargo.sh bench --bench nyc_trip_csv -- --profile-time 20
