\documentclass[fontsize=11pt,a4paper,parskip=half]{scrartcl}

\usepackage{appendix}
\usepackage{booktabs}
\usepackage[style=ieee]{biblatex}
\usepackage[english]{babel}
\usepackage{lmodern,textcomp}
\usepackage[utf8]{inputenc}
\usepackage{csquotes}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{enumitem}
\usepackage[pdftex]{hyperref}
\usepackage{graphicx}
\usepackage{float}
\usepackage{array}

\bibliography{report.bib}

\addtokomafont{labelinglabel}{\sffamily\bfseries}

\makeatletter
\AtBeginDocument{
  \hypersetup{
    pdftitle = {\@title},
    pdfauthor = Gijs van Steenpaal \& Robbert van der Helm,
    pdfsubject = {\@subtitle}
  }
}
\makeatother

\title{DataFusion and Arrow optimization report}
\subtitle{Optimization and Vectorization}
\author{
    Robbert van der Helm\\
    5620724\\
    \small{r.j.helm@students.uu.nl}
\and
    Gijs van Steenpaal\\
    5666503\\
    \small{g.j.vansteenpaal@students.uu.nl}
}

\begin{document}
\maketitle

\section{Optimization goals}

For our optimization project we chose to take a closer look at
\href{https://github.com/apache/arrow/tree/master/rust/datafusion}{DataFusion},
an in-memory distributed query engine created to be a replacement for Apache
Spark which is written in Rust. We chose this project in particular because
we found its use of the Apache Arrow data model very interesting. This model
memory model specifies a standardized language-independent columnar memory
format for flat and hierarchical data, organized for efficient analytic
operations on modern hardware. It provides zero-copy streaming messaging and
interprocess communication.

The DataFusion project was donated to the Apache Arrow in February of this
year\cite{donation} and has since not seen much traction. With a shared
interest between ourselves in both this project and not programming in C++
we found this an interesting challenge. We are not sure if there is much
speed to gain but you never know unless you try and profile~\cite{jacco}.

\subsection{Current state of the project}

We hinted at this earlier but this project is still in development. It has not
reached parity with Apache Spark yet, but the parts that have been implemented
look to be implemented quite well. From our initial exploration it appears that
the code base is already making good use of SIMD and data oriented design. Right
now the query engine sadly does support a few interesting constructs like nested
queries and composite groups. There are plans to implement this and an DataFrame
API alternative but they are not in place yet. We will look at the project as is
and forked it on the 20th of October
\footnote{\url{https://github.com/apache/arrow/commit/34e4ec97b8c786d8ef781c074baacc6742e966b2}}.

\subsection{Benchmarking and setting a baseline}\label{sec:benchmark}

Before we started working on this we decided that our optimizations will be
focused on querying the data and not loading in the data. The main reasoning
behind this is because the data loading process is mainly I/O bound and we
wanted to focus our optimizations on the actual execution of the queries.

Like discussed in the course the first step of optimization is to profile your program.
To do this we needed to find a data set big enough to qualify as \emph{big data}
on which we could preform some interesting queries. What we settled on is the TLC trip
records for January of 2010~\cite{taxi}, this data set consist of every cab ride in New York
city for that month and comes in the form of a 2.6GB CSV file with 15.000.000+ entries.

The queries we use to benchmark are in the form of a simple filter query and one which
preforms a complex aggregate operation, they can be found in the file located at\\
\texttt{./rust/datafusion/benches/nyc\_trip\_csv.rs}.
We run all the benchmarks for a set amount of time, the exact profiling setup is
described in \texttt{./rust/profile.sh}.

\section{High level overview of bottlenecks}

The first step with our initial profiling is a high level analysis of the current
data structures that are in use and the data flow. Like discussed earlier DataFusion
uses Apache Arrow as a data model, we might find some tiny optimization when
looking here but did not bother to deep dive into it since it's sole purpose
of existing is speed. Here we find copious use of SIMD and data neatly arranged
in structures of arrays.

\subsection{Loading data into Arrow}

While this part of the program is not one of our optimization goals we did want
to mention some observations on initializing the data. In our benchmark most time
is spend on part consisting of transforming the data into the arrow memory format.
Profiling found that this operation is currently completely IO bound. We were not
surprised by this, moving a 2.6GB file from a hard disk is anything but free.

We actually timed copying this file and found that this almost took the same
amount of time as loading the file into memory in DataFusion. This took roughly
15 seconds on an NVMe SSD.

To keep our benchmarks clean we kept loading this data outside of our timed tests,
we only need to load in the data once for all the benchmarks since we do not mutate
it. Something similar is done for our profiling runs, we load in the data and then
keep repeating queries. We tell the profiler that there is a warmup period of about
20 seconds in which it does not capture data. This way we can focus on what matters
to us.

\subsection{Query planning phase}

Before running each user query it first needs to get planned, this phase translates
the SQL of the user into operations we preform on the data. This phase is used
to improve the often suboptimal user query and include some optimization outside of
the users control. There are currently two relevant optimizations taking place.

The first is called \emph{Projection push down}, this rule ensures that only the
referenced columns are being traversed during the execution phase. This should
reduce the amount of IO being preformed between memory and the CPU.

The second is \emph{Type Coercion}, this rules will ensure that all binary operators
are working on compatible types. It does this by adding explicit type casts to the
user query. Doing this will keep the runtime query execution code simpler.

Possible additions to this \emph{Predicate push down} and \emph{Limit push down},
these are not added yet but what they will do is basically push down the \texttt{WHERE}
and \texttt{LIMIT} clause down into sub-queries and joins wherever possible. While
this would slow down the planning phase it should improve the execution speed greatly
in some scenarios. The reasoning behind this is that the higher up operations would
be using less data, these rules are crucial in clustered database systems where it
could save network IO. In our case though building it will not gain us any speed,
because of the simple fact that nested queries and joins are not yet supported in
the query engine. Running steps would never change the query planning and will
only take up precious CPU cycles.

The overall time spend on planning is negligible compared to actually executing
the plans, because of this we moved our main focus on the execution phase.

%TODO if we find a simple relevant optimization add it here.

\subsection{Query execution phase}

% We hinted at it before, but this is where the money is. The actual execution
% of the planned queries is the most interesting part. Running the profile we can
% see good use of SIMD and little wasted cycles. But the major take away for our
% initial profiling run was that while it was running fast, it was running on
% on a single core! In one of the next section we will discuss the possibilities
% for multi-threading and the challenges we had implementing it.

DataFusion's query engine operates on what it calls tables, partitions and
batches. A table can consist of multiple partitions, and a partition corresponds
to a single file. Finally, the actual queries are executed in batches. This
makes it possible to operate on files that would be too large to fit in memory.

Right now queries across partitions are executed on parallel, but there is no
parallelism when operating within a batch. This seems like a good first target,
and it is what we will be exploring first.

\section{Null checks}

Our initial profiling showed that most of the cycles were spent on two
functions, namely \texttt{is\_null()} and \texttt{is\_valid()} for a combined 30\%
of the execution time. These functions check whether an element in an array
contains a NULL value or not and they are the complement of each other. These
functions are called repeatedly for every value that has to be added to an
aggregator. The first thing that stood out to us is that this process was
entirely sequential as can be seen in figure~\ref{fig:prof:initial}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{./assets/vtune-before.png}
  \caption{Initial profiling results}\label{fig:prof:initial}
\end{figure}

The actual functions are implemented as a lookup in a bit array. Arrow will fill
this bit array with whether a value with the corresponding index is NULL when
the data gets loaded. This was likely implemented this way to prevent unnecessary
lookups in a cache efficient way.

As can be seen in figure~\ref{fig:prof:initial-callstack}, the
\texttt{is\_valid()} function is called a lot in the
\texttt{GroupedHashAggregateIterator}. This is an iterator that performs
aggregate (i.e. \texttt{MIN} and \texttt{AVG} operations) over groups of
results. Since the entire process is sequential and we are not fully memory
bound yet we felt like we could optimize this process by doing the aggregations
in parallel. This brings us to our next step.

\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{./assets/vtune-before-stacks.png}
  \caption{Initial profiling results with call stacks}\label{fig:prof:initial-callstack}
\end{figure}

% TODO: Zeg iets over de schema

\section{Parallel aggregate}

The aggregation algorithm works as follows. First, the algorithm determines the
group for each row based on the grouping columns. For every unique group it will
build a list of accumulators which are objects that know how to perform an
aggregate operation on a column of data. We then store a pointer to that
aggregator list for every row so that row $i$ uses the list of accumulators with
index $i$. The algorithm then simply loops over all columns and over all rows to
update the accumulator values.

Multithreading this comes with a few challenges. First of all, the current
implementation relies on dynamic dispatch. This makes it easier to add different
aggregator types down the line but it also makes it harder to optimize since we
can't know whether two different accumulators are of the same type without
implementing workarounds. It would have been easier to implement the
accumulators as a sum type (i.e. a tagged union) but that would make it harder
to add new accumulators or operatons in the future it would also make it
impossible to add new accumulators in an external library.

The second problem arises with the way the data is stored in the accumulators.
Storing everything used atomics would have made it relatively cheap to work on
the rows in parallel. Since some of the aggregators such as those used for
summing and calculating averages work with floating point numbers we can't
simply change everything to use atomics unless we rewrite everything for fixed
point arithmetic.

For our initial attempt at parallelizing this we rewrote everything to use
atomic referenced counted pointers and mutexes so we could share the
accumulators between threads. We used
\href{https://github.com/rayon-rs/rayon}{rayon}'s work stealing thread pool and
parallel iterators to work on entire columns at once using a divide and conquer
approach. This showed us that unnecessary locks slow things down by a lot, even
if they are never contested. This can clearly be seen in
figure~\ref{fig:prof:parallel-divide-and-conquer}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{./assets/vtune-parallel-divide-and-conquer.png}
  \caption{Profiler results with divide and conquer approach}\label{fig:prof:parallel-divide-and-conquer}
\end{figure}

After a few iterations we settled on an approach that runs the aggregations in
parallel per group and per column. This avoids acquiring and releasing locks
during the aggregation process and should thus be the optimal parallelization
strategy with the current data setup. From this we quickly discovered that we
were actually limited by the latency of our L1 cache as can be seen in
figure~\ref{fig:prof:parallel-l1-cache-bound}. As mentioned above, the
\texttt{is\_valid()} function performs a simple lookup in a bit array and checks
whether a certain bit is set. As this function is L1 cache bound using multiple
threads does actually not speed up the process, and the overhead from rewriting
our data structure to work with this setup actually slows things down slightly.
This lead us back to the \texttt{is\_valid()} and the \texttt{is\_null()}
functions.

\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{./assets/vtune-parallel-l1-cache-bound.png}
  \caption{Profiler results with divide and conquer approach}\label{fig:prof:parallel-l1-cache-bound}
\end{figure}

% ExecutionContext::collect() uses MergeExec to move the workload from the
% different partitions onto different threads and merges the results back together

\section{Null checks revisited}
After we had parallelized the aggregate query we found that we did not actually
gain any performance. This meant it was time to start profiling again, here we
noticed that we were completely L1 cache bound on null checks. We had all
threads trying to access the same bitmap which stored if indices are null. In an
ideal situation we would want to skip this entirely.

Looking into this option we realized that most of the columns are defined as
non-nullable in the schema. If we had this information when executing the query
we could skip checking for null. Sadly this is only known for the top level
schema. Aggregate query operators such as \texttt{MIN} and \texttt{AVG} actually
construct new columns.

What we did is dive into the query parser, optimizer and its conversion
into an execution plan. Through this we needed to derive if the new
columns would be nullable. How we achieved this is by folding over the
optimized plan and propagating the relevant property into the new columns
\cite{mcco}. The only thing left was to make this now correct schema available
when updating the accumulators. Sadly this is where the fun stopped, we were
unable to get this data here.

We needed to revise our strategy of making this property available, what we
found is that the primitive data types in the Arrow memory model also stored a
private property called null counter. This value has nothing to do with the
table schema but could serve the same purpose. We extended the Arrow
implementation to include a measure of nullability for arrays, and we
consecutively used this information to short circuit the \texttt{is\_valid()}
and \texttt{is\_null()} functions used by DataFusion. Since these functions are
repeatedly called for every value in a column this effectively reduced lots of
lookups in the L1 cache to checking for a single boolean flag. That in turn
yielded a great speed improvement because the work done by the threads was no
longer bounded by the latency of our cache.

What's more, after this optimization we finally saw the parallel version beating
out the sequential version. Since we are now no longer completely memory bound
for operations involving columns marked as \texttt{NOT NULL} we can see that the
parallel processing of grouped columns actually makes a difference. In our
example that difference is only around 30\%, but it could make a much larger
difference when working with more groups or when aggregating over more columns.

% TODO: Do we mention (again) that the current optimization is actually already
%       very efficient? Could be faster with SIMD but only if all rows we wanted
%       to check were sequential.

\section{Avoiding copies}

Next, VTune pointed us to a cloning operation while creating the accumulator
hash map. This was a small implementation detail we even left a comment about.
Making copies only when necessary made everything a few percent faster. We could
not get VTune to show us the same annotated lines again but we felt like it was
worth a mention anyways.

\section{Faster hash maps}

When profiling again we found some more costly lines related to the use of hash
maps during the group operation in \texttt{hash\_aggregate}. The main slowdowns
came from lookup process. This would not be very noticeable on a small scale,
but slow hashing operations millions upon millions of times in a row start to
add up. The default hash map implementation uses the hasher chosen by the type
of the key, and these often prefer a lack of collisions over simply being fast.
Doing some quick research we found that there exists a slightly faster hashing
function called FNV after its authors~\cite{fnv}. It achieves this by giving up
some constrains on being cryptographically sound in favor for being for
performant. This was a rather simple switch since we only had to change the type
of the hash map and it lead to an almost 10\% performance difference.

\section{Final Results}

Sadly this was these were all the optimizations we got around to working on, we
believe we did not leave much performance on the table here. That being said
there is of course still more work to be done once more SQL statements are
supported. But let's talk about our results.

We let our benchmarking suite take 20 samples consisting of 20 iterations for
the 2 benchmark queries and did the same for our optimized version. On average
the complex aggregate query was about 60ms faster and the simple filter query
about 40ms. In figure~\ref{fig:results} you can see the metrics as outputted by
the benchmark, here the baseline is the original unoptimized version. We also
have plots that show how all these runs compare, these can be found in
figure~\ref{fig:comparing}. There is not much to say about the complex query on
the left, but we did have some outliers for the filter bench.

Looking back on this we are very pleased with what we have achieved. The initial
version was already made with performance in mind and especially regarding the
Apache Arrow memory model. But using profiling and some sneaky tricks we were
able to still squeeze more performance out of it.

\begin{figure}[H]
  \centering
  \includegraphics[width=.95\linewidth]{./assets/bench-results.png}
  \caption{Benchmark results comparing queries run in the original to
    our optimized version as described in Section~\ref{sec:benchmark}.}\label{fig:results}
\end{figure}

% \begin{table}[H]
%   \begin{center}
%     \begin{tabular}{l|c|c|c}
%                 & Lower bound & Estimate & Upper bound \\
%       \hline
%       aggregate &	-59.060\% & -58.659\% &	-58.297\% \\
%       simple    & -37.827\% &	-35.349\% &	-32.617\%
%     \end{tabular}
%   \end{center}
%   \caption{Optimized vs original performance for our benchmark over 20 separate runs (p \textless 0.05)}
%   \label{tab:results}
% \end{table}

\begin{figure}[H]
  \centering
  \includegraphics[width=.475\linewidth]{./assets/complex.png}
  \includegraphics[width=.475\linewidth]{./assets/simple.png}
  \caption{Results of 20 runs of the complex (left) and the simple (right) query. Baseline in red, optimized in blue}
  \label{fig:comparing}
\end{figure}

% \section{The simple steps to follow when optimizing}
% \begin{enumerate}[start=0]
%     \item Determine optimization requirements
%     \item Profile: determine hotspots
%     \item Analyze hotspots: determine scalability
%     \item Apply high level optimizations to hotspots
%     \item Profile again.
%     \item Parallelize / vectorize / use GPGPU
%     \item Profile again.
%     \item Apply low level optimizations to hotspots
%     \item Repeat step 6 and 7 until time runs out
%     \item Report.
% \end{enumerate}

% General TODO's for this entire rapport
% 1) Discuss the is_null and is_valid check and the slow down it causes
% by raping the cache and going through data. We need to explain how we
% we get this heuristic on a per batch basis. Via the reader, via the schema
% via data stored in the Array datatype.
% 2) Going in parallel and the fun it brings, the _many_ attempts on the
% road to the working version. Going through the tags.
% 3) With this done we need to make a lot of nice graphs, Jacco likes those
% 4) Profile to final version and discuss what we can and can not improve upon.

\printbibliography
\end{document}
